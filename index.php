<?php
  class message {
    private $text="a simple message";
    public static $count = 0; 
    public function show(){
        echo "<p>$this->text </p>";
    }
    function __construct($text = ""){
        ++self ::$count;
         if($text !=""){
             $this->text = $text;
         }
    }
  }
  ?>
<html>
  <head>
    <title>Object Oriented PHP</title>
  </head>
  <body>
    <p>
    <?php
      $text = "hello world";
      echo "$text and the universe";
      echo "<br>";
      $msg = new message();
      echo "<br>";
      echo message::$count;
      //echo $msg->text;
      $msg->show();
      $msg1 = new message ("a new text");
      $msg1 ->show();
      echo message::$count;
      $msg1 = new message ("");
      $msg1 ->show(); 
      echo "<br>";
      echo message::$count;
      echo "<br>";
      $msg3= new redmessage ("a red message");
      $msg3->show();
      echo "<br>";
      $msg4 = new coloredmessage("a colored message");
      $msg4->color= 'green';
      $msg4->show();
      $msg4->color= 'yellow';
      echo "<br>";
      $msg4->show();
      $msg4->color= 'red';
      echo "<br>";
      $msg4->show();
      $msg4->color= 'black';
      echo "<br>";
      $msg4->show();
      echo "<br>";
      showobject($msg5);
      echo "<br>";

